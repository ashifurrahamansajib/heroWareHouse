# HeroWarehouse - Modern Warehouse Management System


## Description

HeroWarehouse is modern warehouse management system based on web. Our system you can make your warehouse a digital warehouse. Here you can add product and manage your product. You can easily create and store your products to manage it digitally. Make your business digital with us.

## Website Features

- Modern UI
- Add Product
- Delete Product
- Manage Product
- Product Details
- Login
- Register
- Single Page Application

## Used Technologies

- HTML
- CSS 
- Bootstrap
- React Bootstrap
- React
- Node.js
- Express.js
- MongoDB


