import React from 'react';

const Footer = () => {
  return (
    <footer className='brand-color text-white text-center py-3'>
      <div className="container">
        <p className='m-0'>@Copyright &copy; 2024 - Ashifur Rahaman</p>
      </div>
    </footer>
  );
};

export default Footer;